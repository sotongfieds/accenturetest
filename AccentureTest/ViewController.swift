//
//  ViewController.swift
//  AccentureTest
//
//  Created by Hoo Qing Yu on 5/12/18.
//  Copyright © 2018 Hoo Qing Yu. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var currentWeatherIcon: UIImageView!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentMinTempLabel: UILabel!
    @IBOutlet weak var currentMaxTempLabel: UILabel!
    
    @IBOutlet weak var weatherIcon2: UIImageView!
    @IBOutlet weak var weekdayLabel2: UILabel!
    @IBOutlet weak var minTempLabel2: UILabel!
    @IBOutlet weak var maxTempLabel2: UILabel!
    
    @IBOutlet weak var weatherIcon3: UIImageView!
    @IBOutlet weak var weekdayLabel3: UILabel!
    @IBOutlet weak var minTempLabel3: UILabel!
    @IBOutlet weak var maxTempLabel3: UILabel!
    
    @IBOutlet weak var weatherIcon4: UIImageView!
    @IBOutlet weak var weekdayLabel4: UILabel!
    @IBOutlet weak var minTempLabel4: UILabel!
    @IBOutlet weak var maxTempLabel4: UILabel!
    
    @IBOutlet weak var weatherIcon5: UIImageView!
    @IBOutlet weak var weekdayLabel5: UILabel!
    @IBOutlet weak var minTempLabel5: UILabel!
    @IBOutlet weak var maxTempLabel5: UILabel!
    
    // putting various elements in arrays for looping purposes
    var weatherUIArray: [WeatherDayUI] = []
    
    // location managers
    let locationMgr = CLLocationManager()
    
    // interfacing with OpenWeather RESTful APIs
    let weatherURL = "https://openweathermap.org/data/2.5/forecast?appid=b6907d289e10d714a6e88b30761fae22&units=metric&"
    let weatherIconURL = "http://openweathermap.org/img/w/"
    
    let celciusStr = "°C"
    
    let dateFormatter = DateFormatter()
    let displayDateFormatStr = "EEE, dd MMM"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // initialize UI elements array
        weatherUIArray = [
            WeatherDayUI(
                weatherIcon: currentWeatherIcon,
                dayLabel: currentDayLabel,
                minTempLabel: currentMinTempLabel,
                maxTempLabel: currentMaxTempLabel),
            WeatherDayUI(
                weatherIcon: weatherIcon2,
                dayLabel: weekdayLabel2,
                minTempLabel: minTempLabel2,
                maxTempLabel: maxTempLabel2),
            WeatherDayUI(
                weatherIcon: weatherIcon3,
                dayLabel: weekdayLabel3,
                minTempLabel: minTempLabel3,
                maxTempLabel: maxTempLabel3),
            WeatherDayUI(
                weatherIcon: weatherIcon4,
                dayLabel: weekdayLabel4,
                minTempLabel: minTempLabel4,
                maxTempLabel: maxTempLabel4),
            WeatherDayUI(
                weatherIcon: weatherIcon5,
                dayLabel: weekdayLabel5,
                minTempLabel: minTempLabel5,
                maxTempLabel: maxTempLabel5)]
        
        // initialize date formatter
        dateFormatter.dateFormat = displayDateFormatStr
        
        initializeLocationServices()
    }
    
    func reloadWeatherDataUI(data: WeatherData) {
        locationLabel.text = data.city.name
        
        if data.list.count > 0 {
            if data.list[0].weather.count > 0 {
                // update current weather data
                currentWeatherLabel.text = data.list[0].weather[0].description
            }
            
            let currDate = Date()
                
            // time to update all weather data for all days
            for index in (0...(min(weatherUIArray.count, data.list.count) - 1)) {
                let currWeatherDayData = data.list[index]
                if currWeatherDayData.weather.count > 0 {
                    let currWeatherDayWeatherData = currWeatherDayData.weather[0]
                    weatherUIArray[index].weatherIcon.image = UIImage(named: currWeatherDayWeatherData.icon)
                    
                    // 1 day = 86400sec
                    let tarDate = Date(timeInterval: TimeInterval(index * 86400), since: currDate)
                    weatherUIArray[index].dayLabel.text = dateFormatter.string(from: tarDate)
                    
                    weatherUIArray[index].minTempLabel.text = "\(currWeatherDayData.main.temp_min)\(self.celciusStr)"
                    weatherUIArray[index].maxTempLabel.text = "\(currWeatherDayData.main.temp_max)\(self.celciusStr)"
                    
                    weatherUIArray[index].isHidden(flag: false)
                }
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
    
    func initializeLocationServices() {
        // checking for authorisation from user
        self.locationMgr.requestAlwaysAuthorization()
        self.locationMgr.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationMgr.delegate = self;
            locationMgr.desiredAccuracy = kCLLocationAccuracyBest
            locationMgr.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locVal: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("Current location: \(locVal.latitude), \(locVal.longitude)\n");
        getLocationWeatherRawDataBasedOnCoord(lon: locVal.longitude, lat: locVal.latitude)
    }
    
    func getLocationWeatherRawDataBasedOnCoord(lon: Double, lat: Double) {
        guard let url = URL(string: weatherURL + "lat=\(lat)&lon=\(lon)") else { return }
        
        // start urlsession
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            // extend lifetime of data
            guard let data = data else { return }
//            print(data)
//            print("\n\n")
            
            DispatchQueue.main.async {
                self.updateWithWeatherRawData(data: data)
            }
        }.resume()
        // end urlsession
    }
    
    func updateWithWeatherRawData(data: Data) {
        do {
            let weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
            //            print(weatherData)
            self.reloadWeatherDataUI(data: weatherData)
        }
        catch let jsonError {
            print(jsonError)
        }
    }
}
