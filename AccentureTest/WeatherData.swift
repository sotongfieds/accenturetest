//
//  WeatherData.swift
//  AccentureTest
//
//  Created by Hoo Qing Yu on 10/12/18.
//  Copyright © 2018 Hoo Qing Yu. All rights reserved.
//

import Foundation

struct CityDataCoordinates: Codable {
    let lat: Double
    let lon: Double
}

struct CityData: Codable {
    let id: Int
    let name: String
    let coord: CityDataCoordinates
    let country: String
}

struct WeatherDayMainData: Codable {
    let temp: Float
    let temp_min: Float
    let temp_max: Float
    let pressure: Float
    let sea_level: Float
    let grnd_level: Float
    let humidity: Float
    let temp_kf: Float
}

struct WeatherDayWeatherData: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct WeatherDayData: Codable {
    let main: WeatherDayMainData
    let weather: [WeatherDayWeatherData]
    // ignore the rest
}

struct WeatherData: Codable {
    let cod: String
    let message: Float
    let city: CityData
    let list: [WeatherDayData]
}
