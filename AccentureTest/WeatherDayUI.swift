//
//  WeatherDayUI.swift
//  AccentureTest
//
//  Created by Hoo Qing Yu on 9/12/18.
//  Copyright © 2018 Hoo Qing Yu. All rights reserved.
//

import UIKit

class WeatherDayUI {
    var weatherIcon: UIImageView!
    var dayLabel: UILabel!
    var minTempLabel: UILabel!
    var maxTempLabel: UILabel!
    
    init(weatherIcon: UIImageView, dayLabel: UILabel, minTempLabel: UILabel, maxTempLabel: UILabel) {
        self.weatherIcon = weatherIcon
        self.dayLabel = dayLabel
        self.minTempLabel = minTempLabel
        self.maxTempLabel = maxTempLabel
    }
    
    func isHidden(flag: Bool)
    {
        weatherIcon.isHidden = flag
        dayLabel.isHidden = flag
        minTempLabel.isHidden = flag
        maxTempLabel.isHidden = flag
    }
}
