//
//  AccentureTestTests.swift
//  AccentureTestTests
//
//  Created by Hoo Qing Yu on 5/12/18.
//  Copyright © 2018 Hoo Qing Yu. All rights reserved.
//

import XCTest
@testable import AccentureTest

class AccentureTestTests: XCTestCase {

    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        
        // setup code here
        viewController = ViewController()
        
    }

    override func tearDown() {
        viewController = nil
        // calling super function
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            _ = viewController.getLocationWeatherRawDataBasedOnCoord(lon: 1.3521, lat: 103.8198)
        }
    }

}
