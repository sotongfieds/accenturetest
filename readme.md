Hi! Thank you for checking this code out!

A few things to note, I definitely took more than 5 hours to implement, which could be reduced if:

*  If I could gather requirements to have better clarity of what the end product does and looks like. I am also unsure what type of app data would be required to be saved.
*  If art assets were already provided (I did not use the weather icons available on the OpenWeather site as I do not think they will work well on different device screen resolutions, plus all the downloading uses up data and adds loading time, it took a long time to find, download and export the images in various resolutions.)
*  If I know what the final layout is supposed to look like.

With that out of the way, below are some instructions on how to build and test this app!

## Build/Deploy/Test instructions                                                       
1. Please ensure you have the latest XCode installed. You can either download from the App Store, or get it under Downloads from the Apple Developer's portal.
2. Open AccentureTest.xcodeproj that is in the same folder as this file by double-clicking on the file in the Finder.
3. Skip this step if you only wish to test on simulator! You need to have a valid Apple developer account to test on actual device!

    *  To test on an iOS device, first connect the device to your computer.
    *  On the menu bar, left click on Navigate > Reveal In Project Navigator.
    *  On the left tab, left click on AccentureTest at the top. You should see some properties showing up in the middle of XCode.
    *  Left click to select AccentureTest under Targets, and under Signing to the column on right of Targets, ensure "Automatically manage signing" is checked. 
    *  Under Team, select the Team/Developer Account you wish to sign your app with. You may be required to validate your Developer account credentials at this point in time. If you enabled automatic manage signing, XCode will automatically try to generate a provisioning profile to sign the app with.
  
4. To run the build on either device or simulator, on the top bar next to AccentureTest, left-click to select the type of device you wish to target/simulate. Click on the play button on the top left bar, or press Cmd + R to run the app.
5. Alternatively, to run the unit tests for the app, press Cmd + U.
   
## Additional Features To Implement If No Time Limit
1. Come up with a satisfactory layout that allows me to display more information provided through the OpenWeather API, such as humidity, wind etc.
2. Allow user to select the day they wish to see more weather information on. Right now I only display slightly more weather information for the current day. It'll be nice if I have a bottom tab representing all 5 days, and the user can select a particular tab to view the detailed weather information in the remaining top portion of the screen.
3. Make the UI prettier with animated weather .gifs, animated loading animations while the code grabs .json data from the OpenWeather RESTful service etc. Will probably be spending a super long time to be happy with the visuals though, and I had already spent a long time already. It'll be nice to work with a graphics designer for this, it doesn't really make sense for me to be exporting vector art for the various resolutions and experimenting with layouts which my time could be better used to do more coding.
4. Additional feature where user can input the location they wish to check the weather of. Can have long scrolling list of countries, followed by cities for the users to pick.
5. Instead of having fixed layouts, to support cases where more than 5 days worth of weather data were to be displayed, I would strongly advocate for the use of ScrollViews for the days other than today. More UI work, more tweaking of entries with their widths and making sure they translate well for both portrait and landscape modes.
6. If I am really crazy, I can implement cloud animation on top of MapKit/Google Maps. I have not looked into how the data looks like, but there is an demo of the weather map on their website so I know it definitely can be done.
